CREATE TABLE teachers (
    id INT NOT NULL AUTO_INCREMENT,
    teacher_name VARCHAR(55) NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE students(
    id INT NOT NULL AUTO_INCREMENT,
    student_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE courses(
    id INT NOT NULL AUTO_INCREMENT,
    course_name VARCHAR(50) NOT NULL,
    teacher_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_courses_teacher_id
        FOREIGN KEY (teacher_id) REFERENCES teachers(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);
CREATE TABLE student_courses(
    Id INT IDENTITY(1,1) NOT NULL,
    courseId INT NOT NULL,
    studentId INT NOT NULL,
    
    
    CONSTRAINT PK_student_courses_Id PRIMARY KEY CLUSTERED(Id),
    
    CONSTRAINT FK_student_courses_course FOREIGN KEY (courseId)
    REFERENCES courses(Id)
    
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT FK_student_courses_student FOREIGN KEY (studentId)
    REFERENCES students(Id)
    
    ON DELETE CASCADE
    ON UPDATE CASCADE

)